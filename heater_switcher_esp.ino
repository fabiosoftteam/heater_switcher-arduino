#include <Homie.h>
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
//#include <Time.h>
//#include <TimeAlarms.h>

#define kTRUE F("true")
#define kFALSE F("false")
#define kON "on"
#define kOFF "off"

const int PIN_BUTTON = D6;
const int PIN_RELAY_CH1 = D8;

unsigned long buttonDownTime = 0;
byte lastButtonState = 0;
byte buttonPressHandled = 0;

HomieNode heaterNode("heater", "switch");
WiFiUDP ntpUDP;
NTPClient NTPClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);

bool heaterOnHandler(const HomieRange& range, const String& value) {
  if (value != kTRUE && value != kFALSE) return false;
  bool on = (value == kTRUE);
  digitalWrite(PIN_RELAY_CH1, on ? HIGH : LOW);
  heaterNode.setProperty(kON).send(value);
  Homie.getLogger() << F("Light is ") << (on ? kON : kOFF) << endl;
  return true;
}

void toggleRelay() {
  bool on = digitalRead(PIN_RELAY_CH1) == HIGH;
  digitalWrite(PIN_RELAY_CH1, on ? LOW : HIGH);
  heaterNode.setProperty(kON).send(on ? kFALSE : kTRUE);
  Homie.getLogger() << F("Switch is ") << (on ? kON : kOFF) << endl;
}

void Repeats() {
  toggleRelay();
  Homie.getLogger() << F("5 second timer") << endl;
}

void setupHandler() {
  NTPClient.begin();
}

void loopHandler() {
  byte buttonState = digitalRead(PIN_BUTTON);
  if ( buttonState != lastButtonState ) {
    if (buttonState == LOW) {
      buttonDownTime     = millis();
      buttonPressHandled = 0;
    }
    else {
      unsigned long dt = millis() - buttonDownTime;
      if ( dt >= 90 && dt <= 900 && buttonPressHandled == 0 ) {
        toggleRelay();
        buttonPressHandled = 1;
      }
    }
    lastButtonState = buttonState;
  }
  NTPClient.update();
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  pinMode(PIN_BUTTON, INPUT);
  pinMode(PIN_RELAY_CH1, OUTPUT);
  digitalWrite(PIN_RELAY_CH1, LOW);

  Homie_setFirmware("caldaia_controller", "1.0.0");
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);

  heaterNode.advertise(kON).settable(heaterOnHandler);

  Homie.setup();
}

void loop() {

  Homie.loop();
}
